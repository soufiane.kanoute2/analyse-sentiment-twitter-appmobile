
export function getTweetsFromApiWithSearchedText (text) {
  const url = 'https://back-end-ppd.herokuapp.com/api/getTweets/'+ text +'/en?fbclid=IwAR0vXkg6ebImVjtEyGDktU8S23Cp2PiRntyq2pj3PVFWj9zibt0fNdv7S6w'
  return fetch(url)
    .then((response) => response.json())
    .catch((error) => console.error(error))
}