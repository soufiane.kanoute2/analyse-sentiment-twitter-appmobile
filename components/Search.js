import React from 'react'
import {View, Button, TextInput, StyleSheet, FlatList, Text} from 'react-native'
import TweetItem from './Items'
import {getTweetsFromApiWithSearchedText} from '../API/Api'


class Search extends React.Component {

    constructor(props){
        super(props)
        this.state = { ListeTweets : [] }
    }

    _loadTweets() {
        getTweetsFromApiWithSearchedText('covid').then(data => {
            console.log('test de lecture data avec .nbNeg: '+ data.nbNegatif )
            this.setState({ListeTweets:data}
            //console.log('state Tweets:' + this.state.tweets)
            )
        })     
    }

    render(){
            console.log('nouveau rendu')
            console.log('etat du state tweet: '+ this.state.ListeTweets)
            return(
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <View style={styles.top}>
                    <Text style={styles.topText}> PPD - MIAGE20 </Text>
                </View>
                <View style={styles.bottom}>
                    <TextInput style={styles.textinput} placeholder='Titre du film'/>
                    <View style = {styles.buttonContainer}>
                        <Button style={styles.buttonInput} title='Rechercher'  onPress={() => this._loadTweets()}/>
                    </View>            
                </View>
                
                <FlatList
                    data ={ this.state.ListeTweets }
                    //keyExtractor={(item) => item.id.toString()}
                    renderItem={({item}) => <TweetItem tweet={item}/>}
                />
            </View>
            )
    }
}

// Components/Search.js

const styles = StyleSheet.create({
    top:{
        flex: 1,
        backgroundColor: 'blue' ,
        marginTop : 30,
        justifyContent : 'center'
    },
    topText:{
        color: 'white',
        fontFamily: 'sans-serif-medium'
        
    },
    bottom:{
        flex: 9, 
        backgroundColor: 'white',
        justifyContent: 'center'
    },
    compSaisie: {
        
    },
    textinput: {
        marginLeft: 5,
        marginRight: 5,
        height: 50,
        width: 300,
        borderColor: '#000000',
        borderWidth: 1,
        paddingLeft: 5
    },
    buttonContainer:{
        marginLeft: 50,
        marginRight: 50,
        marginTop: 8,
        height: 100
    },
    buttonInput: {
        borderColor: 'blue',
        borderWidth: 1
    }
  })

export default Search