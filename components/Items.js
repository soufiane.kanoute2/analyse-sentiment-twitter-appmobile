// components/Item.js

import React from 'react'
import { StyleSheet, View, Text, Image } from 'react-native'

class Items extends React.Component {

  render() {
    const Ctweets = this.props.tweet
    return (
      <View style={styles.main_container}>
        <Image
          style={styles.image}
          source={ require('../assets/logo-twitter1.png')}
        />
        <View style={styles.content_container}>
          <Text style={styles.title_text}> Ctweets.nbNegatif</Text>
          <View style={styles.description_container}>
            <Text style={styles.description_text} numberOfLines={6}> Ctweets.nbPositif </Text>
            {/* La propriété numberOfLines permet de couper un texte si celui-ci est trop long, il suffit de définir un nombre maximum de ligne */}
          </View>
        </View>
      </View>
    )
  }
}


const styles = StyleSheet.create({
  main_container: {
    height: 190,
    flexDirection: 'row'
  },
  image: {
    width: 100,
    height: 180,
    margin: 5,
    backgroundColor: 'gray'
  },
  content_container: {
    flex: 1,
    margin: 5
  },
  title_text: {
    fontWeight: 'bold',
    fontSize: 20,
    flex: 1,
    flexWrap: 'wrap',
    paddingRight: 5
  },
  description_container: {
    flex: 7
  },
  description_text: {
    fontStyle: 'italic',
    color: '#666666'
  },
})

export default Items